var express = require('express')
var cors = require('cors')
var router = express.Router();

router.use(cors());

var sendgrid = require('sendgrid')('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');
var stripe = require('stripe')('sk_live_UgalBtwAW8D4Y7tAZTXtCQcl');

router.get('/', function(req, res) {
    res.sendStatus(200);
});

router.get('/removeCustomerCard/:customerId', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var customerId = req.params.customerId;

    stripe.customers.del(customerId, function(err, confirmation) {
        var response = {
            "confirmation": confirmation,
            "err": err
        };
        res.send(response);
    });
  } else {
      res.send("Error");
  }
});

router.get('/paymentSubscription/:customerId', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var customerId = req.params.customerId;

    stripe.subscriptions.create({
        customer: customerId,
        plan: "promotion_plan",
    }, function(err, subscription) {
        var response = {
            "isSuccess": true,
            "subscription": subscription
        };
        res.send(response);
    });
  } else {
      res.send("Error");
  }
});

router.get('/paymentChargeByCustomerId/:customerId/:cost', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var customerId = req.params.customerId;
    var cost = req.params.cost + "00";

    stripe.charges.create({
        amount: cost,
        currency: "gbp",
        customer: customerId,
    }).then(function(charge) {
        res.send(charge);
    }).catch(function(err) {
        res.send('Error');
    });
  } else {
      res.send("Error");
  }
});

router.get('/paymentChargeByCustomerIdV2/:customerId/:cost', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var customerId = req.params.customerId;
    var cost = req.params.cost;

    stripe.charges.create({
        amount: cost,
        currency: "gbp",
        customer: customerId,
    }).then(function(charge) {
        res.send(charge);
    }).catch(function(err) {
        res.send('Error');
    });
  } else {
      res.send("Error");
  }
});

router.get('/paymentCreateCustomer/:token/:email', function(req, res) {
  var headerToken = req.get('authorization');

  if (headerToken === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var token = req.params.token;
    var email = req.params.email;

    // Create a Customer Card and return the card id for that customer as reference:
    stripe.customers.create({
        email: email,
        source: token
    }).then(function(customer) {
        res.send(customer);
    });
  } else {
      res.send("Error");
  }
});

router.get('/paymentCreateCustomerAndCharge/:token/:email/:cost', function(req, res) {
  var headerToken = req.get('authorization');

  if (headerToken === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var token = req.params.token;
    var email = req.params.email;
    var cost = req.params.cost + "00";

    // Create a Customer:
    stripe.customers.create({
        email: email,
        source: token
    }).then(function(customer) {
        return stripe.charges.create({
            amount: cost,
            currency: "gbp",
            customer: customer.id
        });
    }).then(function(charge) {
        res.send(charge);
    }).catch(function(err) {
        res.send('Error');
    });
  } else {
      res.send("Error");
  }
});

router.get('/paymentCreateCustomerAndChargeV2/:token/:email/:cost', function(req, res) {
  var headerToken = req.get('authorization');

  if (headerToken === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var token = req.params.token;
    var email = req.params.email;
    var cost = req.params.cost;

    // Create a Customer:
    stripe.customers.create({
        email: email,
        source: token
    }).then(function(customer) {
        return stripe.charges.create({
            amount: cost,
            currency: "gbp",
            customer: customer.id
        });
    }).then(function(charge) {
        res.send(charge);
    }).catch(function(err) {
        res.send(err);
    });
  } else {
      res.send("Error auth poolcover");
  }
});

router.get('/welcomeCompany/:email', function(req, res) {
  var token = req.get('authorization');
  
  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Welcome to PoolCover';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<h3>Welcome,</h3> <br /><p>Thank you for being a part of our community! Hereâ€™s a few suggestions to help get you started:</p><ul><li>Complete your profile - the more information you provide the more chance you have of gaining work.</li><li>Read our FAQs and how it works page for useful insights and top tips.</li><li>Sign up for job alerts and follow us on social media for the latest news.</li></ul><p>Congratulations, youâ€™re now part of a growing network of swim schools becoming part of the Pool Cover.</p><br /><p>Sign into your account: <a href="https://thepoolcover.co.uk">https://thepoolcover.co.uk</a></p><p>Username: <a href="mailto:' + req.params.email + ' "> ' + req.params.email + '</a></p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/welcomeUser/:email', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Welcome to PoolCover';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<h3>Welcome,</h3><br /><p>Thank you for being a part of our community! Hereâ€™s a few suggestions to help get you started:</p><ul><li>Complete your profile - the more information you provide the more chance you have of gaining work.</li><li>Read our FAQs and how it works page for useful insights and top tips.</li><li>Sign up for job alerts and follow us on social media for the latest news.</li></ul><p>Congratulations, youâ€™re now part of a growing network of swimming instructors dedicated to providing casual and part-time cover in the leisure industry. </p><br /><p>Sign into your account: <a href="https://thepoolcover.co.uk">https://thepoolcover.co.uk</a></p><p>Username: <a href="mailto:' + req.params.email + ' "> ' + req.params.email + '</a></p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/appliedSuccess/:email/:name/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Your Job Application - ' + req.params.job + ' ';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>We have received your application for the following vacancy: ' + req.params.job + '. Weâ€™ll be reviewing your application as soon as possible, in the meantime you can view your application at <a href="http://www.thepoolcover.co.uk/applys">http://www.thepoolcover.co.uk/applys</a>.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/createdJob/:email/:name/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job Created - ' + req.params.job + ' ';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Thank you for advertising the following vacancy: ' + req.params.job + '. <br />You can view applicants at any time by logging into <a href="http://www.thepoolcover.co.uk/login">http://www.thepoolcover.co.uk/login</a>.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/jobCompleted/:email/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job Completed - ' + req.params.job + ' ';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Congratulations you have successfully completed the following job: ' + req.params.job + '.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/jobCancelled/:email/:name/:job', function(req, res) {
   var token = req.get('authorization');

   if (token === 'mangospurs') {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Credentials', true);
            res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-Type');

            var email = new sendgrid.Email();
            email.addTo(req.params.email);
            email.subject = 'Job Cancelled - ' + req.params.job + ' ';
            email.from = 'noreply@thepoolcover.co.uk';
            email.html = '<p>Unfortunately the company has cancelled job: ' + req.params.job + '</p>';

            email.addFilter('templates', 'enable', 1);
            email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

            sendgrid.send(email, function(err, json) {
                if (err) {
                    res.send('Error');
                } else {
                    return res.send("Sent");
                }
            });
  } else {
      res.send("Error");
  }
});

router.get('/jobExpired/:email/:name/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job Expired - ' + req.params.job + ' ';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Your job named: ' + req.params.job + ' has now expired and has ran for 28 days. Please visit your job to see the performance stats.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/jobCancelledByCompanyForCompany/:email/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job Cancelled - ' + req.params.job + ' ';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Job: ' + req.params.job + ' has been cancelled.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/jobCancelledByTeacher/:email/:name/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job Cancelled by Teacher';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Unfortunately the Teacher has cancelled their application to the job called: ' + req.params.job + '.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/acceptedForJob/:email/:name/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Accepted for PoolCover job';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>You have successfully been selected for a job called ' + req.params.job + '. Please visit this job on our platform in order to see contact information.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/rejectedForJob/:email/:name/:job', function(req, res) {
  var token = req.get('authorization');
    
  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job application unsuccessful';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>' + req.params.job + '.</p><br /><p>Thank you for your application to "' + req.params.job + '. After careful consideration, we regret to inform you that on this occasion you have been unsuccessful</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/invitedToJob/:email/:name/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job invitation';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>You have been invited to apply for job called: ' + req.params.job + '. Visit your applications and invitations to accept this position if you would like the job.';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/inviteAccepted/:email/:name/:job', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Invite accepted';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Your invitation has been accepted by ' + req.params.name + ' for job called: ' + req.params.job + '. Visit applications to approve them for this position.';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/jobHasApplications/:email/:jobName', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'You have received applications for job: ' + req.params.jobName;
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>You have received applications for job: ' + req.params.jobName + '. <br />Please sign in and review the applications.';
    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/adminJobCreated/:email/:jobName', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Admin: Job Created';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>A job has just been created by a company. Job name:  ' + req.params.jobName + '.';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/adminEnquiry/:email/:userEmail', function(req, res) {
  var token = req.get('authorization');
    
  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Admin: Enquiry';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>An enquiry has been made by this email address: ' + req.params.userEmail + '.';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/jobReceivedMessagesByTeacher/:email/:jobName/:message', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job ' + req.params.jobName + ' has received a message';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Job named: ' + req.params.jobName + ' has received a message, please sign in to respond.<br /><br /> Message received: ' + req.params.message;

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/jobReceivedMessagesByCompany/:email/:jobName/:message', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job ' + req.params.jobName + ' has received a message';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Job named: ' + req.params.jobName + ' has received a message, please sign in to respond.<br />Message received: ' + req.params.message;

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/newJobAdded/:email/:jobName/:jobDescription', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Job Alert: New swimming job';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>A new job is now available in your area. Be first to apply!</p><p>Job description: ' + req.params.jobDescription + '.</p><br /><a href="https://www.thepoolcover.co.uk/search">Click here to view or apply for this job</a>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/emailJobExpiresInOneDay/:jobEmail/:jobName', function(req, res) {
  var token = req.get('authorization');
    
  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.jobEmail);
    email.subject = 'Notification: Job expiring in one day';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Your job called: ' + req.params.jobName + ', is expiring in one day.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/emailJobExpiresInOneWeek/:jobEmail/:jobName', function(req, res) {
  var token = req.get('authorization');
      
  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.jobEmail);
    email.subject = 'Notification: Job expiring in one week';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Your job called: ' + req.params.jobName + ', is expiring in one week.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/nudge/:email/:jobName', function(req, res) {
  var token = req.get('authorization');
    
  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Messages: You have been nudged to respond';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>You have been nudged by job: ' + req.params.jobName + '. Please reply or reject the job.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
      res.send("Error");
  }
});

router.get('/cardRemoved/:email/:cardType', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.email);
    email.subject = 'Billing: Bank card removed';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Your ' + req.params.cardType + ' card has now been removed from our database.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
    res.send("Error");
  }
});

router.get('/emailJobExpired/:jobEmail/:jobName', function(req, res) {
  var token = req.get('authorization');

  if (token === 'mangospurs') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    var email = new sendgrid.Email();
    email.addTo(req.params.jobEmail);
    email.subject = 'Notification: Job expired';
    email.from = 'noreply@thepoolcover.co.uk';
    email.html = '<p>Your job: ' + req.params.jobName + ' has now expired.' +
        'Your opinion is very important to us. We appreciate your feedback and will use it to evaluate changes and make improvements in our site.' +
        '<a title="feedback" href="https://www.surveymonkey.co.uk/r/YZ79BRC">Provide feedback</a>.</p>';

    email.addFilter('templates', 'enable', 1);
    email.addFilter('templates', 'template_id', '43b44c23-74d4-4313-b25a-111faab2759d');

    sendgrid.send(email, function(err, json) {
        if (err) {
            res.send('Error');
        } else {
            return res.send("Sent");
        }
    });
  } else {
    res.send("Error");
  }
});

module.exports = router;